//core
//more about the doc at https://framagit.org/Ariart/bee-color-framework
let WFW = function($webroot,$app_infos){
	let $requiredLibs = [], $loadingLibs = [], $loadedLibs = [], $inited = false;
	let $fnsOnReady = [], $laterPush = [], $toProcess = 0, $init = [];
	$webroot = $webroot.charAt($webroot.length-1) === '/' ? $webroot : $webroot+'/';
	let $loaded = function(){ $fnsOnReady.concat($laterPush).some( ($fn) =>{ return $fn() })};
	let $formatUrl = function($lib){ return $url("JavaScript/"+$lib+".js"); };
	let $redefineError = function(){ throw new Error("Cannot redefine wfw's properties !"); };
	let $addRequired = function($lib){
		if($loadedLibs.indexOf($lib)<0 && $loadingLibs.indexOf($lib)<0) $requiredLibs.push($lib);
	};
	let $createNamespace = function($namespace){
		if(typeof $namespace === "string" && $namespace !== ''){
			let $parts = $namespace.split("/"), $obj = wfw;
			for(let $i = 0; $i < $parts.length-1; $i++){
				if(!($parts[$i] in $obj)){
					let $o = {};
					Object.defineProperty($obj,$parts[$i],{set : $redefineError, get : () => $o });
					$obj = $o;
				}else $obj = $obj[$parts[$i]];
			}
			return { namespace : $obj, name : $parts[$parts.length-1] };
		}else{throw new Error("The namespace have to be a string path.");}
	};
	let $loadRequired = function(){
		$requiredLibs = $requiredLibs.filter((function(){
			let $seen = {};
			return function($element){ return !($element in $seen) && ($seen[$element]=1); }
		})());
		$requiredLibs.slice().forEach(function($lib){
			let $script = document.createElement("script");
			$script.setAttribute("src",$lib);
			$script.setAttribute("type","text/javascript");
			$toProcess++;

			$script.addEventListener("load",function(){
				$toProcess--;
				$loadedLibs.push($lib);
				$loadingLibs.splice($loadingLibs.indexOf($lib),1);
				$script.parentNode.removeChild($script);

				if($loadingLibs.length === 0) {
					if($requiredLibs.length > 0) $loadRequired();
					else if($toProcess === 0 ) $nextInit();
				}
			});
			$script.addEventListener("error",function(){
				$toProcess--; $script.parentNode.removeChild($script);
				throw new Error("Unable to load "+$lib);
			});
			$loadedLibs.push($lib);
			$requiredLibs.splice($requiredLibs.indexOf($lib),1);
			document.body.appendChild($script);
		});
		if($requiredLibs.length === 0 && $toProcess === 0) $nextInit();
	};
	window.addEventListener("load",$loadRequired);

	let $onload = function($fn,$first){ ($first) ? $fnsOnReady.push($fn) : $laterPush.push($fn); };
	let $require = function(...$libs){ $libs.forEach(($elem,$index)=>{
		if(typeof $elem === "string")
			$addRequired($elem.match(/^@.*/)?$elem.replace('@',''):$formatUrl($elem));
		else throw new Error("Require : arg "+$index+" is not a valid lib name !");
	}); };
	let $define = function($namespace,$o,$ovrd){
		let $res = $createNamespace($namespace);
		Object.defineProperty($res.namespace,$res.name,{ get : () => $o,
			set : $redefineError, configurable : $ovrd});
	};
	let $defined = function($path){
		let $o = wfw; let $find = true;
		$path.split("/").forEach(($part)=> $part in $o ? $o=$o[$part] : $find=false);
		return $find;
	};
	let $asyncInit = ($fn)=>{ $init.push($fn); };
	let $nextInit = ()=>{
		if($inited) throw new Error("wfw have already been fully initialized !");
		if($init.length > 0) $init.shift()(); else{ $loaded(); $inited=true }
	};
	let $url = ($path,$cache)=>{ return $webroot+$path+(!$cache?$get('app/cache_burst'):''); };

	let $params=($app_infos) ? JSON.parse($app_infos) : {};
	let $getRef = function($path,$forSet){
		let $o = $params; let $find = true;
		let $paths = (typeof $path === "string") ? $path.split('/') : [];
		let $last = ($forSet !== null)?$paths.pop():null;
		$paths.forEach( ($part) => $find && $part in $o ? $o = $o[$part] : $find = false);
		if($forSet !== null) return {name : $last, o : $o};
		return $find ? $o : null;
	};
	let $get = function($path){
		let $res = $getRef($path,null);
		return ($res !== null) ? JSON.parse(JSON.stringify($res)) : null;
	};
	let $set = function($path,$value){
		let $res = $getRef($path,true);
		if($res !== null) $res.o[$res.name] = $value;
	};
	let $exists = function($path){ return $getRef($path,null) !== null; };
	let $redefineSettingsError = function(){ throw new Error("Cannot redefine settings properties !") };
	let $settings={};
	Object.defineProperties($settings,{
		get : { get : ()=>$get, set : $redefineSettingsError},
		set : { get : ()=>$set, set : $redefineSettingsError},
		exists : { get : ()=>$exists, set : $redefineSettingsError}
	});

	Object.defineProperties(this,{
		ready : { get : () => $onload, set : $redefineError },
		require : { get : () => $require, set : $redefineError },
		define : { get : () => $define, set : $redefineError },
		defined : { get : () => $defined, set : $redefineError },
		webroot : { get : () => $webroot, set : $redefineError },
		init : { get : () => $asyncInit, set : $redefineError },
		next : { get : () => $nextInit, set : $redefineError },
		url : { get : () => $url, set : $redefineError },
		settings : { get : () => $settings, set : $redefineError }
	});
};
let wfw = new WFW(window.webroot ? window.webroot : '/',window.appInfos || '{}');

//node helper
wfw.define("dom/appendTo",function($parent,...$childs){
	$childs.forEach(($child)=>$parent.appendChild($child));
	return $parent;
});
wfw.define("dom/appendCascade",function(...$nodes){
	if($nodes.length < 2 ) throw new Error("At least 2 args required !");
	$nodes = $nodes.reverse(); let $current = $nodes.shift();
	$nodes.forEach(($node)=> {$node.appendChild($current); $current = $node;});
	return $current;
});
wfw.define("dom/insertAfter",function($node, $ref){
	if($ref.nextSibling) $ref.parentNode.insertBefore($node, $ref.nextSibling);
	else $ref.parentNode.appendChild($node);
});
wfw.define("dom/create",function($name, $p){
	let $res = document.createElement($name); $p = ($p && typeof $p === "object") ? $p : {};
	Object.keys($p).forEach(function($key){
		if($key === "data" && $name !== "object"){
			Object.keys($p.data).forEach($k => $res.setAttribute("data-"+$k,$p.data[$k]));
		}else if($key === "className" && Array.isArray($p.className)){
			$p.className.forEach($class => $res.classList.add($class));
		}else if($key === "style"){
			Object.keys($p.style).forEach($k => $res.style[$k]=$p.style[$k]);
		}else if($key === "on"){
			Object.keys($p.on).forEach($k => (Array.isArray($p.on[$k]))
				? $p.on[$k].forEach($j=>$res.addEventListener($k,$p.on[$k][$j]))
				: $res.addEventListener($k,$p.on[$k])
			);
		}else if($key === "options" && $name === "select"){
			Object.keys($p.options).forEach($k => $res.appendChild(wfw.dom.create("option",{
				value : $k, text : $p.options[$k]
			})));
		}else{
			$res[$key]=$p[$key];
		}
	});
	return $res;
});
wfw.define("dom/import/svg",function($url){
	return wfw.dom.create('object',{data:$url,type:"image/svg+xml",on:{load:($o)=>{
		let $svg = $o.target.contentDocument.querySelector("svg");
		$o.target.dispatchEvent(new CustomEvent("svgLoaded",{detail:$svg,bubbles:true}));
		$o.target.parentNode.insertBefore($svg,$o.target);
		$o.target.parentNode.removeChild($o.target);
	}}});
});

//ajax
wfw.define("network/ajax",(function(){
	let $getData = function($d,$append){
		let $res = (!$append) ? '?' : '&';
		$res += Object.keys($d)
			.filter(($k) => { return $d[$k] !== undefined; })
			.map(($k) => { return encodeURIComponent($k) + '=' + encodeURIComponent($d[$k]); })
			.join('&');
		return ($res.length>1) ? $res : '';
	};
	let $postData = function($d,$formData){
		if($d && $d instanceof FormData) return $d;
		$d = ($d && typeof $d === "object") ? $d : {};
		$formData = ($formData instanceof FormData) ? $formData : new FormData();
		Object.keys($d)
			.filter(($key)=>{return $d[$key] !== undefined && $d[$key] !== null})
			.forEach(($k) => {
				if($d[$k] instanceof File) $formData.append($k,$d[$k]);
				else if(Array.isArray($d[$k])) $d[$k].forEach(($elem) => {
					if($elem instanceof File) $formData.append($k,$elem);
					else $formData.append($k,typeof $elem === "object" ? JSON.stringify($elem) : $elem);
				});
				else if(typeof $d[$k] === "object") $formData.append($k,JSON.stringify($d[$k]));
				else $formData.append($k,$d[$k]);
			});
		return $formData;
	};
	return function($url,$params){
		let $req = {};
		$req.type = ('type' in $params) ? $params.type.toUpperCase() : "GET";
		$req.headers = ('headers' in $params) ? $params.headers : {};
		$req.withCredentials = ('withCredentials' in $params) ? $params.withCredentials : false;
		$req.appendGetData = ('appendGetData' in $params) ? $params.appendGetData : false;
		$req.appendPostData = ('appendPostParams' in $params) ? $params.appendPostData : false;
		//Data
		$req.data = ('data' in $params) ? $params.data : null;
		$req.getData = ('getData' in $params) ? $params.getData : {};
		$req.getData = $getData($req.getData,$req.appendGetData);
		$req.postData = ('postData' in $params) ? $params.postData : null;
		if($req.appendPostData) $req.postData = $postData($req.postData,$req.appendPostData);
		else $req.postData = $postData($req.postData);
		//Callbacks
		$req.error = (typeof $params.error === 'function') ? $params.error : null;
		$req.onload = (typeof $params.onload === 'function') ? $params.onload : null;
		$req.success = (typeof $params.success === 'function') ? $params.success : null;
		$req.beforeSend = (typeof $params.beforeSend === 'function') ? $params.beforeSend : null;
		$req.onreadystatechange = (typeof $params.onreadystatechange === 'function')
			? $params.onreadystatechange : null;

		let $xhr = new XMLHttpRequest();
		if($req.onreadystatechange) $xhr.onreadystatechange = $req.onreadystatechange;
		$xhr.open($req.type,$url.replace(/^\/+/,"/")+$req.getData);
		Object.keys($req.headers).forEach(($key) => $xhr.setRequestHeader($key,$req.headers[$key]));
		$xhr.onload = ($req.onload) ? $req.onload($xhr) : function(){
			if($xhr.status >= 200 && $xhr.status < 400){
				if($req.success) $req.success($xhr.responseText,$xhr);
			}else{ if($req.error) $req.error($xhr.responseText,$xhr); }
		};
		$xhr.onerror = ()=>{ if($req.error) $req.error($xhr.responseText,$xhr); };
		if($req.beforeSend) $req.beforeSend($xhr);
		if($req.type !== 'GET') $xhr.send($req.postData ? $req.postData : $req.data);
		else $xhr.send();
	};
})());
wfw.define("network/wfwAPI",(function($csrfToken){
	return function($url,$params){
		$params = ($params) ? $params : {};
		let $success = (typeof $params.success === 'function') ? $params.success : null;
		let $error = (typeof $params.error === 'function') ? $params.error : ()=>null;
		(typeof $params.getData === "object")
			? $params.getData.ajax=true : $params.getData={ajax:true};
		if($csrfToken && !('csrfToken' in $params.getData)) $params.getData.csrfToken = $csrfToken;

		$params.success = function($response){
			let $code = "-1"; let $res = null;
			try{
				let $resp = JSON.parse($response); $code = $resp.code; $res = $resp.data;
			}catch($err){
				if(!$error) throw new Error("Unreadable response : "+$response);
				else $error($response);
				return undefined;
			}
			if($code && typeof $params[$code] === 'function') $params[$code]($res);
			else if($code && $code.toString().charAt(0) !== '0') $error($res,$code);
			else if($success) $success($res,$code);
		};
		wfw.network.ajax($url,$params);
	};
})(window.csrfToken || null));

wfw.define("ui/loaders/eclipse",function($message, $doc){
	let $style; let $span; $doc = $doc || document;
	let $loader = wfw.dom.appendTo(wfw.dom.create("div",{className:"eclipse-loader"}),
		wfw.dom.create("div",{className:"loader"}),
		$span = wfw.dom.create("p",{className:"message",innerHTML:$message})
	);
	let $updateMessage = ($message)=>{ $span.innerHTML = $message; };
	if(!($style = $doc.head.querySelector('#eclipse-loader-css')))
		$doc.head.appendChild($style = wfw.dom.create('style',{id:"eclipse-loader-css", innerHTML: `
			 .eclipse-loader {position:relative;margin:auto;}
			 .eclipse-loader .loader{
				border-radius:100px;
				border-top:3px solid var(--bc-barcodes-color);
				border-bottom:3px solid var(--bc-barcodes-color);
				animation-name: eclipse-rotate;
				animation-iteration-count:infinite;
				animation-duration:1s;
				margin:auto;
				height:100px;
				width:100px;
				position:relative;
			 }
			 .eclispe-loader .message{margin:0;padding:0;text-align:center;}
			 @keyframes eclipse-rotate{
				from{transform:rotate(0deg);}
				to{transform:rotate(360deg);}
			 }`
		}));
	let $del=()=>{throw new Error("This loader have been deleted and can't be used anymore !");};
	let $remove = ()=>{
		if($loader.parentNode) $loader.parentNode.removeChild($loader);
		$loader = undefined; $updateMessage = $del; $remove = $del;
	};
	let $redefineError = ()=>{ throw new Error("Cann't redefine eclipse's properties !") };
	Object.defineProperties($loader,{
		wfw : { get : ()=>this, set : $redefineError}
	});
	Object.defineProperties(this,{
		html : { get : ()=>$loader, set : $redefineError },
		updateMessage : { get : ()=>$updateMessage, set : $redefineError },
		delete : { get : ()=>$remove, set : $redefineError }
	});
});

wfw.ready(()=>{
	let $panel, $gtinI, $body, $csrf, $categories,  $parent = document.querySelector(".page-wrapper");
	let $url = /*@*/"/admin/BeeColor/barcodes/index.php"/*@*/, $ready = false, $error = null, $searchResult = null;
	let $formatNumber = ($n)=>{
		return (new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' })).format($n);
	};
	let $stateCheck = ()=>{
		if(!$ready){
			if(!$error) alert("Le module n'est pas encore prêt, veuillez patienter...");
			else alert(
				"Le module a rencontré une erreur et n'a pas été correctement intialisé :\n\n"
				+$error
				+"\n\n Merci de recharger la page.\nSi le problème persiste, contactez votre prestataire."
			);
		}
		return $ready;
	};
	let $showError = ($message)=>{ alert($message); $error = $message; };
	let $openPanel = ()=>{
		if(!$panel.parentNode){
			$parent.appendChild($panel);
			document.body.classList.add("barcodes-opened"); document.body.scrollIntoView();
		}
	};
	let $closePanel = ()=>{
		if($panel.parentNode){
			$parent.removeChild($panel);
			document.body.classList.remove("barcodes-opened");
		}
	};
	let $displayLoader = ($message, $displayProgress)=>{
		let $loader = new wfw.ui.loaders.eclipse($message,document); let $progress;
		if($displayProgress){
			$progress = wfw.dom.create("div",{className:"progress"});
			$loader.html.querySelector(".loader").appendChild($progress);
		}
		let $shadowLoader = wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-loader"}),
			wfw.dom.appendTo(wfw.dom.create("div",{className:"container"}),$loader.html)
		);
		$body.appendChild($shadowLoader);
		return {
			loader : $loader, progress : $progress,
			remove : ()=>{ $loader.delete(); $shadowLoader.parentNode.removeChild($shadowLoader); }
		};
	};
	let $createCategoyItem = ($id,$data)=>{
		let $openClose, $res;
		return $res = wfw.dom.appendTo(wfw.dom.create("div",{id:"barcodes-categ-"+$id, className:"barcodes-categ-item"+($data.parent?"":" barcodes-protected")}),
			wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-categs-head"}),
				wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-categs-input-wrapper"}),
					wfw.dom.create("input",{type:"checkbox",id:"categ-"+$id,className:"categs",data:{cid:$id}})
				),
				wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-categs-label-wrapper"}),
					wfw.dom.create("label",{htmlFor:"categ-"+$id,innerHTML:$data.name}),
					$openClose = wfw.dom.create("span",{className:"barcodes-categs-opener",innerHTML:"&#128448;",on:{click:()=>{
						if($res.classList.contains("opened")){
							$openClose.innerHTML = '&#128448;';
							$res.classList.remove("opened");
						}else{
							$openClose.innerHTML = '&#128449;';
							$res.classList.add("opened");
						}
					}}})
				),
			),
			wfw.dom.create("div",{className:"child-list"})
		);
	};
	let $categoriesList = ()=>{
		let $categs = wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-categories"}));
		Object.keys($categories).forEach(($id)=>{
			let $p = $categs;
			if($categories[$id].parent){
				$p = $categs.querySelector("#barcodes-categ-"+$categories[$id].parent+" .child-list");
			}
			$p.appendChild($createCategoyItem($id,$categories[$id]));
		});
		Array.from($categs.querySelectorAll(".barcodes-categ-item")).forEach($c=>{
			if($c.childNodes[$c.childNodes.length - 1].childNodes.length === 0)
				$c.querySelector(".barcodes-categs-opener").style.display="none";
		});
		return $categs;
	};
	let $choose = ($params,$choice) => {
		let $modified = false, $chooser, $title, $desc, $price, $brand, $sku, $qtt;
		wfw.dom.appendTo($body,
			$chooser = wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-choosed-window"}),
				wfw.dom.appendTo(wfw.dom.create("form",{className:"barcodes-choosed-form",on:{change:()=>$modified = true}}),
					wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-input-wrapper"}),
						wfw.dom.create("p",{innerHTML:"Titre* :"}),
						$title = wfw.dom.create("input",{name:"title",className:$params.title?"":"required",value:$params.title?$params.title:''}),
					),
					wfw.dom.appendTo(wfw.dom.create("div",{className:"flex"}),
						wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-input-wrapper"}),
							wfw.dom.create("p",{innerHTML:"SKU :"}),
							$sku = wfw.dom.create("input",{name:"sku",value:$params.sku?$params.sku:''})
						),
						wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-input-wrapper"}),
							wfw.dom.create("p",{innerHTML:"MARQUE* :"}),
							$brand = wfw.dom.create("input",{name:"brand",className:$params.brand?"":"required",value:$params.brand?$params.brand:''})
						),
						wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-input-wrapper"}),
							wfw.dom.create("p",{innerHTML:"PRIX* :"}),
							$price = wfw.dom.create("input",{name:"price",className:$params.price?"":"required",value:$params.price?$params.price:''})
						),
						wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-input-wrapper"}),
							wfw.dom.create("p",{innerHTML:"QUANTITÉ* :"}),
							$qtt = wfw.dom.create("input",{name:"quantity",value:$params.qtt?$params.qtt:1})
						)
					),
					wfw.dom.appendTo(wfw.dom.create("div",{className:"flex barcodes-columns"}),
						wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-input-wrapper barcodes-categories-input required"}),
							wfw.dom.create("p",{innerHTML:"Catégories* :"}),
							$categoriesList()
						),
						wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-input-wrapper"}),
							wfw.dom.create("p",{innerHTML:"Description* :"}),
							$desc = wfw.dom.create("textarea",{name:"desc",className:$params.desc?"":"required",value:$params.desc?$params.desc:''})
						)
					)
				),
				wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-choosed-actions"}),
					wfw.dom.create("div",{className:"barcodes-button",innerHTML:"Annuler",on:{click:()=>{
						if($modified){
							if(!confirm("Toutes vos modifications seront perdues. Souhaitez-vous continuer ?"))
								return;
						}
						$chooser.parentNode.removeChild($chooser);
					}}}),
					wfw.dom.create("div",{className:"barcodes-button",innerHTML:"Créer le produit",on:{click: ()=>{
						let $categs = Array.from(document.querySelectorAll("input.categs"))
							.filter($i => $i.checked).map($i => Number.parseInt($i.getAttribute("data-cid")))
							.filter($n => !isNaN($n) && $n.toString().length < 10);
						let $data = {
							title : $title.value, description : $desc.value, price : $price.value, quantity : $qtt.value,
							brand : $brand.value, sku : $sku.value, "categories[]" : $categs, choosen:$choice
						};
						let $validation = true;
						if($categs.length === 0){
							$validation = false;
							alert("Vous devez choisir au moins une catégorie de produits !");
							document.querySelector(".barcodes-categories-input").classList.add("required");
						}else document.querySelector(".barcodes-categories-input").classList.remove("required");
						if($data.price.toString().length === 0){
							$validation = false;
							alert("Vous devez préciser le prix !");
							$price.classList.add("required");
						}else $price.classList.remove("required");
						if($data.title.length === 0){
							$validation = false;
							alert("Vous devez préciser un titre !");
							$title.classList.add("required");
						}else $title.classList.remove("required");
						if($data.description.length === 0){
							$validation = false;
							alert("Vous devez préciser une description !");
							$desc.classList.add("required");
						}else $desc.classList.remove("required");
						if($data.brand.length === 0){
							$validation = false;
							alert("Vous devez préciser la marque du produit !");
							$brand.classList.add("required");
						}else $qtt.classList.remove("required");
							if($data.quantity.length === 0){
								$validation = false;
								alert("Vous devez préciser une quantité !");
								$qtt.classList.add("required");
							}else $qtt.classList.remove("required");
						if(!$validation) return;
						let $createLoader = $displayLoader("Création du produit en cours, veuillez patienter...");
						wfw.network.wfwAPI($url,{
							getData : { action: "create", csrfToken: $csrf, gtin: $params.gtin },
							type : "POST",
							postData : $data,
							"001" : $data => {
								$createLoader.remove();
								let $confirm;
								wfw.dom.appendTo($body,
									$confirm = wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-created-product"}),
										wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-created-wrapper"}),
											wfw.dom.create("p",{innerHTML : "Le produit a bien été créé !"}),
											wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-choosed-actions"}),
												wfw.dom.create("a",{
													className:"barcodes-button",href:$data,
													innerHTML:"Voir et/ou modifier", target:"_blank"
												}),
												wfw.dom.create("div",{className:"barcodes-button", innerHTML:"Quitter",on:{click: ()=>{
													$chooser.parentNode.removeChild($chooser);
													$confirm.parentNode.removeChild($confirm);
													if($searchResult)
														$searchResult.parentNode.removeChild($searchResult);
													$gtinI.value = '';
													$gtinI.focus();
												}}})
											)
										)
									)
								);
							},
							error : $data => {
								$createLoader.remove();
								alert("Une erreur est survenue lors de la création du produit :\n"+$data);
							}
						});
					}}})
				)
			)
		);
	};
	let $createProductResults = $products => {
		let $res = [];
		$products.forEach(($p,$i)=>{
			if(!('images' in  $p)) $p.images = [];
			$res.push(wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-product"}),
				wfw.dom.create("h3",{className:"barcodes-product-title",innerHTML : $p.title}),
				wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-product-infos"}),
					wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-product-refs"}),
						wfw.dom.create("span",{innerHTML : "<span>GTIN : </span><span>"+$p.gtin+"</span>"}),
						wfw.dom.create("span",{innerHTML : "<span>SKU : </span><span>"+($p.sku ? $p.sku : "Non trouvé")+"</span>"})
					),
					wfw.dom.create("p",{className:"barcodes-product-brand",innerHTML : "<span>MARQUE : </span><span>"+$p.brand+"</span>"}),
					wfw.dom.appendTo(wfw.dom.create("p",{className:"barcodes-product-prices"}),
						wfw.dom.create("span",{innerHTML:"PRIX :&nbsp;"}),
						wfw.dom.create("span",{className:"barcodes-product-ht",innerHTML:$formatNumber($p.price)+" HT"}),
						wfw.dom.create("span",{className:"barcodes-product-ttc",innerHTML:$formatNumber($p.price_including_vat)+" TTC"}),
						wfw.dom.create("span",{className:"barcodes-product-rate",innerHTML:" (TVA : "+$p.tax_rate+"%)"})
					)
				),
				wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-product-content"}),
					wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-product-pics"}),
						...$p.images.map($i=>{
							return wfw.dom.create("img",{src:$i})
						})
					),
					wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-product-desc"}),
						wfw.dom.create("div",{innerHTML : $p.desc})
					),
					wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-buttons"}),
						wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-source"}),
							wfw.dom.create("a",{
								href:$p.found_at, target:"_blank", className:"barcodes-button",
								innerHTML:"Voir sur "+$p.found_on.replace("https://","")
							})
						),
						wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-choose"}),
							wfw.dom.create("div",{className:"barcodes-button",innerHTML:"Choisir",on:{click:() =>{
								$p.choosed = $i; $choose($p, $i);
							}}})
						)
					)
				)
			));
		});
		return $res;
	};
	let $createSearchResultContainer = ($params)=>{
		let $new = typeof $params !== "string";
		return wfw.dom.appendTo(wfw.dom.create("section",{className:"search-results"}),
			wfw.dom.appendTo(wfw.dom.create("div",{className:"search-results-head"}),
				wfw.dom.create("h2",{innerHTML : $new ? $params.length+" produit(s) trouvé(s) en ligne" : "Ce produit existe déjà dans votre base de données !"})
			),
			wfw.dom.appendTo(wfw.dom.create("div",{className:"search-results-body"}),
				...($new
					? $createProductResults($params)
					: [wfw.dom.create("a",{
						className:"barcodes-button exists", innerHTML:"Voir et/ou modifier",
						target: "_blank", href: $params
					})]
				)
			)
		);
	};
	let $check = ()=>{
		if($stateCheck()){
			let $v = $gtinI.value;
			if($v.length === 0){
				alert("Vous devez entrer un code-barres !");
				return;
			}
			$v = $v.trim().replace(/[ _-]/,'');
			if(!$v.match(/^[0-9]{4,20}$/)){
				if($v.length < 4) alert("Ce code-barres est trop court (taille minimale : 4 chiffres) !");
				else if($v.length > 20) alert("Ce code-barres est trop long (taille maximale : 20 chiffres) !");
				else alert("Un code-barres ne doit comporter que des chiffres !");
				return;
			}
			let $prev = document.querySelector(".search-results");
			if($prev) $prev.parentNode.removeChild($prev);
			let $loader = $displayLoader("Recherche d'informations sur le produit en cours, veuillez patienter...<br>(Cette opération peut prendre un certain temps)");
			wfw.network.wfwAPI($url,{
				getData : {action : 'check', csrfToken : $csrf, gtin : $v},
				"001" : ($data)=>{
					$loader.remove();
					$body.appendChild($searchResult = $createSearchResultContainer($data));
				},
				"002" : ($data)=>{
					$loader.remove();
					$body.appendChild($searchResult = $createSearchResultContainer($data));
				},
				error : ($data)=>{
					alert("Une erreur est survenue durant la recherche :\n"+$data);
					$loader.remove();
				}
			});
		}
	};
	let $hiddenFile = null;
	let $hiddenForm = null;
	let $import = ()=>{
		let $f = $hiddenFile.files[0];
		if(!$f.type.match(/text\/csv|application\/vnd\.ms-excel/i)){
			alert("Votre fichier doit être un fichier csv !");
		}else{
			let $loader = $displayLoader("Import de votre fichier en cours, veuillez patienter...", true);
			wfw.network.wfwAPI($url,{
				getData : { action : "import", csrfToken : $csrf },
				postData : { import_csv : $f },
				type:"POST",
				beforeSend:($xhr)=>{$xhr.upload.addEventListener("progress",($e)=>{
					$loader.progress.innerHTML='<span>'+Math.round(($e.loaded*100)/$e.total)+'%</span>';
				})},
				"000" : ()=>{
					setTimeout(()=>alert("Fichier importé avec succés !"),1);
					$loader.remove();
					$hiddenForm.reset();
				},
				error : ($data) => {
					setTimeout(()=>alert("Une erreur est survenue lors de l'import de votre fichier : \n"+$data),1);
					$loader.remove();
					$hiddenForm.reset();
				}
			});
		}
	};
	$hiddenFile = wfw.dom.create("input",{type:"file",className:"hidden-input",on:{change:$import}});
	$panel = wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-window"}),
		wfw.dom.create("div",{className:"barcodes-bg",on:{click:$closePanel}}),
		wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-panel"}),
			wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-header"}),
				wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-title"}),
					wfw.dom.create("h2",{innerHTML:"Gestionnaire de codes-barres"})
				),
				wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-close-wrapper"}),
					wfw.dom.create("div",{
						className:"barcodes-close",title:"Fermer",innerHTML:"+",on:{click:$closePanel}
					})
				)
			),
			$body = wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-body"}),
				wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-content"}),
					wfw.dom.appendTo(wfw.dom.create("div",{className:"barcodes-check"}),
						$gtinI = wfw.dom.create("input",{placeholder:"Entrez un code-barres",on:{
							keydown : $e =>{ $e.keyCode === 13 ? $check() : null }
						}}),
						wfw.dom.create("div",{innerHTML:"Rechercher",className:"barcodes-button",on:{click:$check}}),
						$hiddenForm = wfw.dom.appendTo(wfw.dom.create("form",{className:"hidden-input"}),$hiddenFile),
						wfw.dom.create("div",{innerHTML:"&#9660;",className:"import barcodes-button",
							title:"Importer une base GTIN/SKU",on:{click:()=>$hiddenFile.click()}
						})
					)
				),
			)
		)
	);
	wfw.dom.appendTo(document.querySelector(".page-main-actions"), wfw.dom.create("div",{
		className:"page-actions-buttons action-barcodes", on : { click : $openPanel},
		innerHTML : "<span>Codes-barres</span>"
	}));
	let $baseMessage = "Initialisation en cours, veuillez patienter...<br>";
	let $initLoader = $displayLoader($baseMessage+"(1/2) Récupération de la clé de sécurité...");
	wfw.network.wfwAPI($url,{
		getData : {action : "connect"},
		"001" : $data =>{
			$csrf = $data;
			$initLoader.loader.updateMessage($baseMessage+"(2/2) Récupération de la liste des catégories de produit...");
			wfw.network.wfwAPI($url,{
				getData : {action : 'categories', csrfToken : $csrf},
				"001" : ($data)=> {
					$categories = $data; $initLoader.remove(); $ready = true;
					setInterval(()=>wfw.network.wfwAPI($url,{
						getData : {action : 'keep_alive', csrfToken : $csrf},
						403 : ()=>{
							alert("Vous avez été déconnecté !");
							window.location.reload(true);
						},
						error : $data => console.log($data)
					}),60000);
				},
				error : $data => {
					console.log($data);
					$showError("Module Code-barres Error :\nImpossible de récupérer la liste des catégories produit !\n"+$data)
				}
			});
		},
		error : $data => {
			console.log($data); $initLoader.remove();
			$showError("Module Code-barres Error :\nImpossible de récupérer la clé de sécurité !\n"+$data);
		}
	})
});