<?php

define("DATA_ROOT",dirname(__DIR__)."/data");
define("MAGENTO_ROOT",dirname(__DIR__,3));

//init
session_start();
$sessionCookie = $_COOKIE["admin"] ?? null;
$defaultSession = $_COOKIE["PHPSESSID"] ?? null;

//Only define some basic libs
$confs = require_once __DIR__."/private/config/conf.php";
require_once __DIR__."/private/lib/basics.php";

//app
try{
	if((!$sessionCookie || empty($sessionCookie)) && !isset($_SESSION["admin"]["user"])){
		$error(ERR_403);
	}else{
		if($sessionCookie){
			if($defaultSession && file_exists(session_save_path()."/sess_$defaultSession")){
				session_decode(file_get_contents(session_save_path()."/sess_$defaultSession"));
				if(isset($_SESSION["admin"]["updated_at"])) $_SESSION["admin"]["updated_at"] = time();
				session_write_close();
				session_start();
			}
			session_decode(file_get_contents(session_save_path()."/sess_$sessionCookie"));
		}
		if(!isset($_SESSION["admin"]["user"])) $error(ERR_403,WARN);
		if(isset($_SESSION["admin"]["updated_at"])) $_SESSION["admin"]["updated_at"] = time();

		$validateAndSanitize(ACTION, CSRF);
		$action = $_GET[ACTION];
		$csrf = $_GET[CSRF] ?? null;

		//Set up magento environment
		require_once MAGENTO_ROOT.'/app/bootstrap.php';
		$bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
		$_objectManager = $bootstrap->getObjectManager();

		switch($action){
			case ACTION_CONNECT :
			case ACTION_CHECK :
			case ACTION_CREATE :
			case ACTION_IMPORT :
			case ACTION_CLEAR_CACHE :
			case ACTION_CATEGORIES :
			case ACTION_KEEP_ALIVE :
				require_once __DIR__."/private/actions/$action.php";
				break;
			default : $error(ERR_404,ERR); break;
		}
	}
}catch(Error | Exception $e){
	$error([ "code" => ERR_500["code"], "data" => ERR_500["data"]." : $e" ], FATAL);
}