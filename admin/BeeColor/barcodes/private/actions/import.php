<?php

require_once dirname(__DIR__)."/lib/importGTINMap.fn.php";

$validateAndSanitize(IMPORT_CSV,CSV_DELIMITER,CSV_GTIN_INDEX, CSV_SKU_INDEX);
move_uploaded_file(
	$_FILES[IMPORT_CSV]["tmp_name"],
	$file = dirname(__DIR__."/data/imported.csv")
);
$importGTINMap(
	$file,
	$_POST[CSV_DELIMITER] ?? $confs["GTINMap"]["csv"]["delimiter"] ?? ',',
	$_POST[CSV_GTIN_INDEX] ?? $confs["GTINMap"]["csv"]["gtin_index"] ?? 0,
	$_POST[CSV_SKU_INDEX] ?? $confs["GTINMap"]["csv"]["sku_index"] ?? 1
);
unlink($file);
$respond("000","Done");