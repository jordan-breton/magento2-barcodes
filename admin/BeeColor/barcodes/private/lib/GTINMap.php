<?php

$GTINMapPath = $confs["GTINMap"]["path"] ?? $confs["data"]."/gtin_to_sku_map.php";

if(!file_exists($GTINMapPath))
	file_put_contents($GTINMapPath,"<?php\n\nreturn [];\n");

$GTINToSKU = require_once $GTINMapPath;
$SKUToGTIN = array_flip($GTINToSKU);

$getSKUByGTIN = function(string $gtin) use(&$GTINToSKU):?string{
	return $GTINToSKU[$gtin] ?? null;
};
$getGTINBySKU = function(string $sku) use(&$SKUToGTIN):?string{
	return $SKUToGTIN[$sku] ?? null;
};
$addToGTINMap = function(string $gtin, string $sku) use (&$SKUToGTIN, &$GTINToSKU):void{
	$SKUToGTIN[$sku] = $gtin;
	$GTINToSKU[$gtin] = $sku;
};

/**
 * Save the GTIN to SKU map into a file, ignoring all invalid values.
 * @return bool True if all lines have been written, false if any line have been skipped.
 */
$saveGTINMap = function() use (&$log,&$GTINToSKU,&$GTINMapPath):bool{
	$writtenLines = 0; $nbLines = count($GTINToSKU);
	$log("Starting GTIN => SKU map saving... ($nbLines items)");
	$res = "<?php\n\nreturn [";
	foreach($GTINToSKU as $gtin => $sku){
		if(is_scalar($gtin) && is_scalar($sku)){
			$res.="\t\"$gtin\" => \"$sku\"".(($writtenLines < $nbLines)?',':'')."\n";
			$writtenLines++;
		}else $log("Invalid value found at offset $gtin : ".print_r($sku,true),ERR);
	}
	$res.= "];\n";
	file_put_contents($GTINMapPath,$res);
	$log("GTIN => SKU map saved ($writtenLines items saved)",OK);
	return $writtenLines === $nbLines;
};