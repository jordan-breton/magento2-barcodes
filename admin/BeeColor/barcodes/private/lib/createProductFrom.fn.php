<?php

require_once __DIR__."/GTINMap.php";

define("TITLE","title");
define("PRICE","price");
define("BRAND","brand");
define("QTT","quantity");
define("SKU","sku");
define("DESC","description");
define("CHOOSEN","choosen");
define("CATEGS","categories");

$additionnalValidationKeys[TITLE] = function() use(&$error) {
	$title = $_POST[TITLE] ?? null;
	if(!is_string($title) || strlen($title) > 255)
		$error([ "code" => 400, "data" => "A title must be specified (POST) and must not exceed 255 chars."]);
	$_POST[TITLE] = htmlentities(strip_tags($title));
};
$additionnalValidationKeys[DESC] = function() use(&$error) {
	$desc = $_POST[DESC] ?? null;
	if(!is_string($desc) || strlen($desc) > 10000)
		$error([ "code" => 400, "data" => "A description must be specified (POST) and must not exceed 10000 chars."]);
};
$additionnalValidationKeys[BRAND] = function() use(&$error) {
	$brand = $_POST[BRAND] ?? null;
	if(!is_string($brand) || strlen($brand) > 255)
		$error([ "code" => 400, "data" => "A brand name must be specified (POST) and must not exceed 255 chars."]);
	$_POST[BRAND] = htmlentities(strip_tags($brand));
};
$additionnalValidationKeys[PRICE] = function() use(&$error) {
	$price = $_POST[PRICE] ?? null;
	if(!is_string($price) || !preg_match("/^[0-9]{1,}((\.|,)[0-9]{1,}|)$/",$price))
		$error([ "code" => 400, "data" => "A price must be specified (POST) and must be a valid floating number."]);
	$_POST[PRICE] = floatval(str_replace(",",".",$price));
};
$additionnalValidationKeys[QTT] = function() use(&$error){
	$qtt = $_POST[QTT] ?? null;
	if(!is_string($qtt) || !preg_match("/^[0-9]+$/",$qtt))
		$error(["code" => 400, "data"=>"A quantity must be specified (POST) and must be a valid integer !"]);
	$_POST[QTT] = intval($qtt);
};
$additionnalValidationKeys[SKU] = function() use(&$sku) {
	$sku = $_POST[SKU] ?? null;
	if(empty($sku)) $sku = null;
	if(is_string($sku) && !preg_match("/^.{1,255}$/",$sku))
		$error([ "code" => 400, "data" => "A SKU, if specified, must be a string smaller than 255 chars !" ]);
	$_POST[SKU] = $sku ? htmlentities(strip_tags($sku)) : null;
};
$additionnalValidationKeys[CHOOSEN] =  function() use (&$error){
	$choosen = $_POST[CHOOSEN] ?? null;
	if(is_string($choosen) && !preg_match("/^[0-9]+$/",$choosen))
		$error([ "code"=>400, "data"=>"You must pick one !" ]);
	$_POST[CHOOSEN] = $choosen ? intval($choosen) : null;
};
$additionnalValidationKeys[CATEGS] =  function() use (&$error){
	$categs = $_POST[CATEGS] ?? null;
	if(is_array($categs)){
		foreach($categs as $k=>$v){
			if(!preg_match("/^[0-9]+$/",$v)) $error([
				"code" => 400, "data" => "All values must be integers ! (error at offset $k)"
			]); else $categs[$k] = intval($v);
		}
	}
	$_POST[CATEGS] = $categs ? $categs : null;
};

/**
 * Create a product in the magento store.
 *
 * @param             $_objectManager Magento's object manager.
 * @param string      $gtin           Product's GTIN (will be used as SKU)
 * @param string      $title          Product's title
 * @param string      $desc           Product's description
 * @param string      $price          Product's price
 * @param string      $brand          Product's brand
 * @param int|null    $choosen        Choosen product, if a product have been found online.
 *                                    (Allow to retrieve product's images)
 * @param null|string $sku            product's SKU. If not given, GTIN will be set as SKU.
 * @param array|null  $categories     Categories the product must belong to.
 * @param int         $qtt
 * @return mixed Created product
 */
$createProductFrom = function(
	$_objectManager,
	string $gtin,
	string $title,
	string $desc,
	string $price,
	string $brand,
	?int $choosen = null,
	?string $sku = null,
	?array $categories = [],
	int $qtt = 1
) use (&$addToGTINMap, &$saveGTINMap, &$log, &$confs){
	$brand = str_replace(["_"," "],"-",ucfirst(strtolower($brand)));
	/** @var PDO $resource */
	$resource = $_objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection();
	$statement = $resource->prepare('SELECT brand_id FROM ves_brand WHERE name LIKE ?');
	$statement->execute([$brand]);
	$res = $statement->fetchAll(PDO::FETCH_ASSOC);
	$brand = html_entity_decode($brand);
	$brandId = null;
	if(count($res) === 0){
		$log("Brand $brand not found. Attempting to create new one...",WARN);
		$query = $resource->prepare(
			'INSERT INTO ves_brand (name,url_key,creation_time,page_layout,status,position,group_id) VALUES (?,?,?,?,?,?,?)'
		);
		$query->execute([
			$brand,
			urlencode(strtolower($brand)),
			date('Y-m-d H:i:s',time()),
			'empty', 1, 0, 3
		]);
		$brandId = $resource->lastInsertId();
		if(!is_null($brandId)) $log("New brand ame created.",OK);
		else throw new Exception("Unable to create new brand (no id returned after INSERT)");
	}else $brandId = $res[0]["brand_id"];

	$sku = "BP-".strtoupper($brand).'-'.($sku ?? $gtin);

	$state = $_objectManager->get('Magento\Framework\App\State');
	$state->setAreaCode('frontend');

	$title = html_entity_decode($title);
	$desc = html_entity_decode($desc);
	$simpleProduct = $_objectManager->create('\Magento\Catalog\Model\Product');
	$simpleProduct->setSku($sku);
	$simpleProduct->setName($title);
	$simpleProduct->setMetaTitle($title);
	$simpleProduct->setMetaDescription($desc);
	$simpleProduct->setShortDescription($desc);
	$simpleProduct->setDescription($desc);
	$simpleProduct->setStatus(1);
	$simpleProduct->setTaxClassId(12);
	$simpleProduct->setTypeId('simple');
	$simpleProduct->setPrice($price);
	$simpleProduct->setCreatedAt(time());
	$simpleProduct->setWebsiteIds([1]);
	$simpleProduct->setVisibility(4);
	$simpleProduct->setAttributeSetId(4);
	$simpleProduct->setUrlKey(urlKey(($sku ?? $gtin)."-$title"));
	$simpleProduct->setMediaGallery(["images"=>[],"values"=>[]]);
	$simpleProduct->setStockData([
		"qty" => $qtt,
		"manage_stock" => true,
		"use_config_manage_stock" => false,
		"backorders" => 0,
		"is_in_stock" => true
	]);

	if(is_array($categories) && count($categories) > 0)
		$simpleProduct->setCategoryIds($categories);
	$toRemove = [];
	if(!is_null($choosen) && isset($_SESSION[RETRIEVED_PRODUCTS][$gtin][$choosen])){
		$pics = $_SESSION[RETRIEVED_PRODUCTS][$gtin][$choosen];
		$toFlag = 0;
		foreach($pics as $k=>$pic){
			$flags = [];
			if($k === $toFlag) $flags = ["image","thumbnail","small_image","swatch"];
			$mediaPath = MAGENTO_ROOT."/pub/media/".basename($pic);
			rename(MAGENTO_ROOT."/$pic",$mediaPath);
			$simpleProduct->addImageToMediaGallery($mediaPath,$flags,true,false);
			$toRemove[] = $mediaPath;
			$toRemove[] = MAGENTO_ROOT."/$pic";
		}
	}
	$simpleProduct->save();
	$simpleProductId = $simpleProduct->getId();

	$statement = $resource->prepare('INSERT INTO ves_brand_product (brand_id, product_id) VALUES (?,?)');
	$statement->execute([$brandId,$simpleProductId]);

	if($sku && $sku !== $gtin){
		$addToGTINMap($gtin,$sku);
		$saveGTINMap();
	}

	$productsCache = $confs["cache"]["products_cache"] ?? dirname(__DIR__)."/products_cache";
	if(file_exists("$productsCache/$gtin")) unlink("$productsCache/$gtin");
	foreach($toRemove as $file){
		if(is_file($file)) unlink($file);
	}
	if(isset($_SESSION[RETRIEVED_PRODUCTS][$gtin])) unset($_SESSION[RETRIEVED_PRODUCTS][$gtin]);

	return $simpleProduct;
};