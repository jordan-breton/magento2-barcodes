<?php

/**
 * Find any product information thanks to the GTIN provided by the user.
 * @param string $gtin GTIN of the product to be searched online
 * @return array List of products that have been found online.
 */
$searchProductOnline = function(string $gtin) use (
	&$log, &$confs, &$cacheHTML, &$fetchHTMLFromCache, &$clearHTMLCache, &$downloadRemoteFile
) : array{
	$finalRes = [];
	$imagesPath = dirname(__DIR__,2)."/Images";
	if(!is_dir($imagesPath)) mkdir($imagesPath);
	$productsCache = $confs["cache"]["products_cache"] ?? dirname(__DIR__)."/products_cache";
	if(!is_dir($productsCache)) mkdir($productsCache);
	if(file_exists("$productsCache/$gtin") && microtime(true) - filemtime("$productsCache/$gtin") < $confs["cache"]["max_products_lifetime"] ?? 86400){
		$res = json_decode(file_get_contents("$productsCache/$gtin"),true);
		if(!isset($_SESSION[RETRIEVED_PRODUCTS])) $_SESSION[RETRIEVED_PRODUCTS] = [];
		if(!isset($_SESSION[RETRIEVED_PRODUCTS][(string) $gtin]))
			$_SESSION[RETRIEVED_PRODUCTS][(string) $gtin] = [];
		foreach($res as $productInfos){
			$_SESSION[RETRIEVED_PRODUCTS][(string) $gtin][] = $productInfos["images"];
		}
		$log("Products fetched from cache",OK);
		return $res;
	}
	try{
		foreach($confs["search"] as $k=>$remoteConf){
			$log("Starting GTIN search for $k...");
			$searchUrl = $remoteConf["domain"].str_replace("[gtin]",$gtin,$remoteConf["url"]);
			if(! ($results = $fetchHTMLFromCache($searchUrl))){
				$results = $downloadRemoteFile($searchUrl);
				if(!empty($results)){
					$log("Result page found at $searchUrl", OK);
					$cacheHTML($searchUrl, $results);
				}else{
					$log("Unable to reach $searchUrl",ERR);
					continue;
				}
			}

			libxml_use_internal_errors(true);
			$log("Parsing page to find result...");
			$doc = new DOMDocument();
			$doc->loadHTML($results);
			$path = new DOMXPath($doc);
			$nodes = [];
			foreach($remoteConf["XPath"]["productList"] as $xpathQuery){
				$nodes = $path->query($xpathQuery);
				if(count($nodes) === 0) continue;
			}
			if(count($nodes) === 0){
				$log("No result found at $searchUrl for GTIN $gtin.", ERR);
				continue;
			}
			foreach($nodes as $no=>$node){
				if($no >= $remoteConf["max_products"]) break;
				$productUrl = $node->getAttribute("href");
				if(strpos($productUrl,'/') === 0){
					$productUrl = $remoteConf["domain"].$productUrl;
					if(!($productPage = $fetchHTMLFromCache($productUrl))){
						$productPage = $downloadRemoteFile($productUrl);
						if(!empty($productPage)){
							$log("Product page found at $searchUrl", OK);
							$cacheHTML($productUrl,$productPage);
						}else{
							$log("Unable to reach $searchUrl",ERR);
							continue;
						}
					}

					$log("Searching product infos on product page...");
					$productDoc = new DOMDocument();
					$productDoc->loadHTML($productPage);
					$productPath = new DOMXPath($productDoc);
					$productInfos = [];
					foreach($remoteConf["XPath"]["productInfos"] as $attr => $queries){
						$tmpNode = null;
						foreach($queries as $query){
							$queryResult = $productPath->query($query);
							if(count($queryResult) === 0) continue;
							switch($attr){
								case "price" :
									$productInfos[$attr] = floatval(
										str_replace(",",".",sanitizeString($queryResult->item(0)->textContent))
									);
									$taxRate = $remoteConf["tax_rate"] ?? $confs["tax_rate"] ?? 20;
									$productInfos["price_including_vat"] = $productInfos[$attr];
									$productInfos["tax_rate"] = $taxRate;
									$productInfos[$attr] /= (1+($taxRate/100));
									break;
								case "images" :
									if(!isset($productInfos[$attr])) $productInfos[$attr] = [];
									foreach($queryResult as $tmpNode){
										if(strtolower($tmpNode->nodeName) === "script"){
											$log("Parsing script to find pictures...");
											if(isset($remoteConf["script_parsers"]["images"]) && is_callable($remoteConf["script_parsers"]["images"])){
												$productInfos[$attr] = array_merge(
													$productInfos[$attr],
													$remoteConf["script_parsers"]["images"]($tmpNode->textContent)
												);
											}
											else $log("No script parser defined for images. Please check your configuration !",ERR);
										}else{
											$src = $tmpNode->getAttribute("src");
											if(!is_bool(strpos($src,";base64,"))){
												$log("Base64 encoded image ignored...",WARN);
											}else $productInfos[$attr][] = sanitizeString($tmpNode->getAttribute("src"));
										}
									}
									break;
								case "desc" :
									$productInfos[$attr] = preg_replace('/<!--(.|\s)*?-->/', '', sanitizeString(array_reduce(
										iterator_to_array($queryResult->item(0)->childNodes),
										function ($carry, \DOMNode $child) {
											return $carry.$child->ownerDocument->saveHTML($child);
										}
									)));
									break 2;
								case "brand" :
									$productInfos[$attr] = ucfirst(strtolower(
										sanitizeString($queryResult->item(0)->textContent)
									));
									break 2;
								default :
									$productInfos[$attr] = sanitizeString($queryResult->item(0)->textContent);
									break 2;
							}
						}
					}
					if(!empty($productInfos)){
						$log("Some infos have been found for result $no.",OK);
						$productInfos["gtin"] = $gtin;
						$productInfos["found_on"] = $remoteConf["domain"];
						$productInfos["found_at"] = $productUrl;

						if(!isset($_SESSION[RETRIEVED_PRODUCTS])) $_SESSION[RETRIEVED_PRODUCTS] = [];
						if(!isset($_SESSION[RETRIEVED_PRODUCTS][(string) $gtin]))
							$_SESSION[RETRIEVED_PRODUCTS][(string) $gtin] = [];
						if(isset($productInfos["images"]) && count($productInfos["images"]) > 0){
							foreach($productInfos["images"] as $k=>$url){
								$ext = explode(".",$url);
								$ext = array_pop($ext);
								$picName = sha1("$gtin-$k-".str_replace("%","",basename($url)));
								$picUrl = "$imagesPath/$picName.$ext";
								try{
									$downloadRemoteFile($url,$picUrl);
									$log("Picture at $url saved to $picUrl",OK);
									$productInfos["images"][$k] = str_replace(MAGENTO_ROOT,"",$picUrl);
								}catch(Error | Exception $e){
									$log("Unable to download $url : $e",ERR);
								}
							}
							$_SESSION[RETRIEVED_PRODUCTS][(string) $gtin][] = $productInfos["images"];
						}
						$finalRes[] = $productInfos;
					}else $log("No infos found for result $no.",ERR);
				}
			}
			if(count($finalRes) > 0){
				$log("Products found on ".$remoteConf["domain"],OK);
				break;
			}else $log("Product not found on ".$remoteConf["domain"],ERR);
		}
	}catch(Error | Exception $e){
		$clearHTMLCache();
		throw $e;
	}
	file_put_contents("$productsCache/$gtin",json_encode($finalRes));
	return $finalRes;
};