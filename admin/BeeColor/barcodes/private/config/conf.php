<?php
return [
	"data" => "/root/BeeColor/barcodes",
	"cache" => [
		"max_products_lifetime" => 86400,
		"max_html_lifetime" => 86400,
		"products_cache" => dirname(__DIR__)."/products_cache",
		"html_cache" => dirname(__DIR__)."/html_cache"
	],
	"GTINMap" => [
		"path" => dirname(__DIR__)."/data/gtin_to_sku_map.php"
	],
	"logs" => [
		"enabled" => true,
		"path" => dirname(__DIR__)."/data/barcodes.log",
		"WARN" => [ "color" => 33, "text" => "WARN" ],
		"LOG" => [ "color" => 0, "text" => "INFO" ],//0 : default
		"ERR" => [ "color" => 31, "text" => "ERR" ],//31 : rouge
		"FATAL" => [ "color" => 41, "text" => "FATAL ERROR" ],//41 : beackground rouge
		"OK" => ["color" => 32, "text" => "SUCESS"],//32 : vert
		"LOG_TEMPLATE" => "\e[@colorm[@date] [@ip] [@type] @message\e[0m\n"
	],
	"search" => [
		"amazon" => [
			"domain" => "https://amazon.fr",
			"url" => "/s?k=[gtin]",
			"max_products" => 5,
			"tax_rate" => 20,
			"script_parsers" => [
				"images" => function(string $script)use(&$log):array{
					$res = [];
					$start = strpos($script,"var data");
					$end = strpos($script,"};");
					if(is_bool($start) || is_bool($end)){
						return [];
					}
					$var = substr($script, $start, $end + 1 - $start);
					$var = preg_replace("/( ){2,}/"," ",$var);
					$var = str_replace("\n","",$var);
					$tmp = str_split($var);
					$i = array_search("{",$tmp);
					if(!is_bool($i)){
						$tmp = array_splice($tmp,$i);
						$json = implode('',$tmp);
						$json = str_replace("'","\"",$json);
						$json = str_replace(array("\n","\r"),"",$json);
						$json = preg_replace('/([{,]+)(\s*)([^"]+?)\s*:/','$1"$3":',$json);
						$json = preg_replace('/(,)\s*}$/','}',$json);
						$data = json_decode($json,true);
						if(!is_null($data) && is_array($data["colorImages"]["initial"] ?? null)){
							foreach($data["colorImages"]["initial"] as $picInfos){
								$res[] = array_keys(array_reverse($picInfos["main"]))[0];
							}
						}
					}
					return $res;
				}
			],
			"XPath" => [
				"productList" => [
					"//*[contains(@class,'s-result-list') and contains(@class, 's-search-results')]"
					."/div[not(contains(@class,'AdHolder'))]//h2//a[contains(@class, 'a-link-normal')]"
				],
				"productInfos" => [
					"title" => ["//*[contains(@id,'productTitle')]"],
					"brand" => ["//*[contains(@id,'bylineInfo')]//a"],
					"desc" => [
						"//div[contains(@id,'productDescription_feature_div')]/div[contains(@id,'productDescription')]"
						."/div[contains(@id,'productDescription')]"
					],
					"price" => ["//span[contains(@id,'priceblock_ourprice')]"],
					"images" => [
						"//*[contains(@id,'imageBlock')]//*[not(contains(@id,'altImages'))]"
						."//ul/li[contains(@class,'image') and contains(@class,'item')]//img",
						"//div[contains(@id,'imageBlock_feature_div')]/div[contains(@id,'imageBlock')]/following::script[1]"
					],
					"sku" => ["//*[text()='Modèle fabricant']/following::td[contains(@class,'value')]"]
				]
			]
		]
	]
];